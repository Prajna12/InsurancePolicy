package com.Service.InsuranceClaim;

import com.model.Insurance.InformationInsurance;

public interface InsuranceClaimService {
	public double calculateInsuranceClaim(InformationInsurance informationInsurance);
}
