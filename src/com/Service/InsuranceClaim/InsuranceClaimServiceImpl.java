package com.Service.InsuranceClaim;

import com.model.Insurance.InformationInsurance;

public class InsuranceClaimServiceImpl  implements InsuranceClaimService{
  
private static int basePremium = 5000; 
	
	@Override
	public double calculateInsuranceClaim (InformationInsurance  informationInsurance)
	{

		
		totalPremiumAmt = getCurrentHealthPremium(insurancePremiumDetails.getCurrentHealth(), totalPremiumAmt);
		
		totalPremiumAmt = getHabitPremium(insurancePremiumDetails.getHabits(), totalPremiumAmt);	
		
		
		return totalPremiumAmt;
	}
	
	public double getPremiumAmtForAge(int age, double totalPremiumAmt) {
		
		if(age < 18) {
			totalPremiumAmt = basePremium;
		} else if(age >= 18 && age < 25) {
			totalPremiumAmt = getPremiumAmt(1);
		} else if(age >= 25 && age < 30) {
			totalPremiumAmt = getPremiumAmt(2);
		} else if(age >= 30 && age < 35) {
			totalPremiumAmt = getPremiumAmt(3);
		} else if(age >= 35 && age < 40) {
			totalPremiumAmt = getPremiumAmt(4);			
		} else if(age >= 40) {
			totalPremiumAmt = getPremiumAmt(5);
			int ageDiff = age - 40;
			int percent = ageDiff/5;
			
			for(int i = 1; i < percent; i++) {
				totalPremiumAmt = totalPremiumAmt + ((totalPremiumAmt*20)/100);
			}
		}	
		
		return totalPremiumAmt;
	}
	
	public double getPremiumAmt(int n) {
		for(int i = 1; i <= n; i++) {
			if(i < 5) {
				basePremium = basePremium + ((basePremium*10)/100);
			} else {
				basePremium = basePremium + ((basePremium*20)/100);
			}
			
		}
		return basePremium;		
	}
	}
		
}

