package com.model.Insurance;

public class InformationInsurance 
{
	
	private String name;
	private Gender gender;
	private int age;
	private Insurance insurance;
	private Habits habits;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Insurance getInsurance() {
		return insurance;
	}
	public void setCurrentHealth(Insurance currentHealth) {
		this.insurance = insurance;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
}

