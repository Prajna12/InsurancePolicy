package com.model.Insurance;

public class Insurance {

	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;
	
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isbloodPressure() {
		return bloodPressure;
	}
	
	public void setbloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isbloodSugar() {
		return bloodSugar;
	}
	
	public void setbloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverWeight() {
		return overWeight;
	}
	
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}
	
}
