package com.model.Insurance;

public class Habits {
	private boolean smoking;
	private boolean alcohol;
	private boolean dailyExercise;
	private boolean drugs;
	
	public boolean issmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isalcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol= alcohol;
	}

}
